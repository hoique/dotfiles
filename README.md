# Dotfiles
---
Dotfiles for my Linux setup

#### Required software
* bspwm
* sxhkd
* picom
* polybar
* rxvt-unicode
* rofi
* feh
* zsh
* ZSH plugin: zsh-syntax-highlighting
* ZSH plugin: zsh-autosuggestions
---
#### Required fonts
* Iosevka SS02
* Noto CJK
